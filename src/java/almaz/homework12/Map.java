package almaz.homework12;

public interface Map<K, V> {
    // положить значение value, по ключу key
    void put(K key, V value);
    // получить значение по ключу
    V get(K key);
}
