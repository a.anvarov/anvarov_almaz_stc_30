package almaz.homework12;

public class HashMapImpl<K, V> implements Map<K, V> {

    private static final int DEFAULT_SIZE = 16;

    private MapEntry<K, V> entries[] = new MapEntry[DEFAULT_SIZE];

    private static class MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    // TODO: лучше ознакомься с приницпом работы хэштаблиц, и смыслом методов hashCode() и equals()

    // TODO: нужно реализовать обработку колизий
    // put("Марсель", 27);
    // put("Марсель", 28);
    // у меня сохранятся оба значения, вам нужно сделать, чтобы хранилось только одно
    @Override
    public void put(K key, V value) {
        MapEntry<K, V> newMapEntry = new MapEntry<>(key, value);

        int index = key.hashCode() & (entries.length - 1);

        entries[index] = newMapEntry;
    }

    // TODO: нужно реализовать обработку колизий
    @Override
    public V get(K key) {
        for (MapEntry entry : entries) {
            if (entry != null && entry.key.equals(key)) {
                return (V) entry.value;
            }
        }
        return null;
    }
}
