package almaz.homework12;


// коллекция, которая гарантирует уникальность своих значений
    // TODO: реализовать класс HashSet, который будет использовать HashMap
public interface Set<V> {
    void add(V value);
    boolean contains(V value);
}
