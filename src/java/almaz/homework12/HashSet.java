package almaz.homework12;

public class HashSet<E> implements Set<E> {
    private static final int DEFAULT_SIZE = 16;
    private final int[] entryHash = new int[DEFAULT_SIZE];
    Map<E, Object> map = new HashMapImpl<>();
    int index = 0;

    private int hash(E key) {
        return key.hashCode();
    }


    // TODO почиатй про паттерн адаптер, ты очень усложнил код
    @Override
    public void add(E key) {
        int hashNum = hash(key);
        if (!contains(key)) {
            entryHash[index] = hashNum;
            map.put(key, hashNum);
            index++;
        }
    }

    @Override
    public boolean contains(E key) {
        for (int hash : entryHash) {
            if (hash == key.hashCode() && (!key.equals(map.get(key)))) {
                return true;
            }
        }

        return false;
    }
}
