package almaz.homework12;

public class Main {

    public static void main(String[] args) {
        Map<String, Integer> map = new HashMapImpl<>();

        map.put("Марсель", 26);
        map.put("Марсель", 27);
        map.put("Марсель", 28);
        map.put("Денис", 30);
        map.put("Илья", 28);
        map.put("Неля", 18);
        map.put("Катерина", 23);
        map.put("Полина", 18);
        map.put("Регина", 18);
        map.put("Максим", 18);
        map.put("Сергей", 18);
        map.put("Иван", 18);
        map.put("Виктор", 18);
        map.put("Виктор Александрович", 18);

        System.out.println(map.get("Марсель"));
        System.out.println(map.get("Денис"));
        System.out.println(map.get("Илья"));
        System.out.println(map.get("Неля"));
        System.out.println(map.get("Катерина"));
        System.out.println(map.get("Полина"));
        System.out.println(map.get("Регина"));
        System.out.println(map.get("Алмаз"));

        int i = 0;
    }
}
