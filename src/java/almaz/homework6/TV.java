package almaz.homework6;

import java.util.Random;

/**
 * телевизор
 */
public class TV {

    private String name;

    /**
     * телевизионные каналы
     */
    private Channel channels[];

    public TV() {
    }

    public TV(String name, Channel[] channels) {
        this.name = name;
        this.channels = channels;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Channel[] getChannels() {
        return channels;
    }

    public void setChannels(Channel[] channels) {
        this.channels = channels;
    }

    /**
     * показывает канал по номеру
     *
     * @param channelNumber номер канала
     * @return название передачи, если канал не настроен будет выведена соответсвующее сообщение на экране
     */
    public String showProgramByChanelNumber(int channelNumber) {
        if (channelNumber <= channels.length) {
            Random random = new Random();
            Program[] programs = channels[channelNumber].getPrograms();
            return programs[random.nextInt(programs.length)].getName();
        } else {
            return "Канала под № " + channelNumber + " не настроен";
        }
    }
}
