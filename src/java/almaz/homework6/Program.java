package almaz.homework6;

/**
 * телевизионная программа
 */
public class Program {

    /**
     * наименование программы
     */
    private String name;

    public Program() {
    }

    public Program(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
