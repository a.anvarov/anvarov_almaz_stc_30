package almaz.homework6;

/**
 * удаленное управление телевизором
 */
public class RemoteController {

    /**
     * объект телевизора
     */
    TV tv;

    public RemoteController() {
    }

    public RemoteController(TV tv) {
        this.tv = tv;
    }

    public TV getTv() {
        return tv;
    }

    public void setTv(TV tv) {
        this.tv = tv;
    }

}
