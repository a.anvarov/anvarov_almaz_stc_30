package almaz.homework6;

/**
 * телевизионные каналы
 */
public class Channel {

    /**
     * наименование канала
     */
    private String name;

    /**
     * список программ телевизионного канала
     */
    private Program programs[];

    public Channel() {
    }

    public Channel(String name, Program[] programs) {
        this.name = name;
        this.programs = programs;
    }

    public Program[] getPrograms() {
        return programs;
    }

    public void setPrograms(Program[] programs) {
        this.programs = programs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
