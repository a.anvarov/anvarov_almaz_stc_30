package almaz.homework6;

/**
 * демонстрирует работу удаленного управления телефизором
 */
public class Main {

    /**
     * метод запускает демонстрацию работу удаленного управления телевизором
     *
     * @param args массив аргументтов
     */
    public static void main(String[] args) {


        Program[] programs = {
                new Program("Галилео"),
                new Program("Top Gear"),
                new Program("История"),
                new Program("История 19 век"),
                new Program("История 18 век"),
                new Program("История 16 век"),
                new Program("История 14 век"),
                new Program("Футбол РПЛ"),
                new Program("Футбол АПЛ"),
                new Program("Музыка топ 10")
        };

        Channel channel1 = new Channel("стс", programs);
        Channel channel2 = new Channel("стс-hd", programs);
        Channel channel3 = new Channel("стс-fullhd", programs);

        Channel[] channels = {channel1, channel2, channel3};

        TV tv = new TV();
        tv.setChannels(channels);
        tv.setName("LG");

        RemoteController remoteController = new RemoteController(tv);

        for (int i = 0; i < channels.length; i++) {
            System.out.println(remoteController.getTv().showProgramByChanelNumber(i));
        }
    }

}
