package almaz.homework10;

public interface Iterable {
    Iterator iterator();
}
