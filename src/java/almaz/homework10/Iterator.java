package almaz.homework10;

/**
 * итерируется по элементам коллекции
 */
public interface Iterator {

    /**
     * возвращает следующий элемент
     *
     * @return
     */
    int next();

    /**
     * проверяет, есть ли следующий элемент
     *
     * @return
     */
    boolean hasNext();
}