package almaz.homework10;

public interface Collection extends Iterable {
    void add(int element);
    boolean contains(int element);
    int size();
    void removeFirst(int element);
}
