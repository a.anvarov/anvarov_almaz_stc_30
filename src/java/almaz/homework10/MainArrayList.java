package almaz.homework10;

public class MainArrayList {

    public static void main(String[] args) {
        List list = new ArrayList();

        for (int i = 0; i < 17; i++) {
            list.add(i);
        }

        list.removeFirst(8);
        list.removeFirst(100);

        System.out.println(list.get(8));
        System.out.println(list.get(100));

        System.out.println(list.indexOf(11));
        System.out.println(list.indexOf(100));

        System.out.println(list.size());

        System.out.println(list.contains(15));
        System.out.println(list.contains(100));

        list.removeByIndex(2);
        list.removeByIndex(0);
        list.removeByIndex(-2);
        list.removeByIndex(100);

        list.insert(4, 2);
        list.insert(0, 0);
        list.insert(100, 100);

        System.out.println("Прверка итератора");

        Iterator iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        System.out.println();
    }
}
