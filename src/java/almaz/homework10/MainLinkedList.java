package almaz.homework10;

public class MainLinkedList {

    public static void main(String[] args) {

        List list = new LinkedList();

        for (int i = 0; i < 17; i++) {
            list.add(i);
        }

        list.add(18);
        list.add(100);

        list.insert(200, 0);

        list.removeByIndex(0);
        list.removeByIndex(15);

        list.removeFirst(2);
        list.removeFirst(10);
        list.removeFirst(999);

        list.insert(100, 100);
        list.insert(50, 50);

        ((LinkedList) list).reverse();

        System.out.println(list.get(0));
        System.out.println(list.get(1));
        System.out.println(list.get(2));

        System.out.println(list.contains(777));

        list.indexOf(5);

        System.out.println(list.contains(6));

        System.out.println("Прверка итератора");

        Iterator iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        System.out.println();
    }
}
