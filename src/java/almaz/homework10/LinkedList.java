package almaz.homework10;

/**
 * список на основе связанного списка
 */
public class LinkedList implements List {

    private Node first;
    private Node last;

    private int count;

    private static class Node {
        int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }
    }

    /**
     * разворачивает элементы в списке в обратном порядке
     */
    public void reverse() {
        Node previousNode = null;
        Node currentNode = first;

        while (currentNode != null) {
            Node nextNode = currentNode.next;
            currentNode.next = previousNode;
            previousNode = currentNode;
            currentNode = nextNode;
        }

        Node temp = first;
        first = last;
        last = temp;
    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);

        if (first == null) {
            first = newNode;
        } else {
            last.next = newNode;
        }

        last = newNode;
        count++;
    }

    @Override
    public boolean contains(int element) {
        Node current = this.first;

        while (current != null) {
            if (current.value == element) {
                return true;
            }
            current = current.next;
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeFirst(int element) {
        Node previousNode = null;
        Node currentNode = first;

        while (currentNode != null) {
            if (currentNode.value == element) {
                previousNode.next = currentNode.next;
                count--;
            }
            previousNode = currentNode;
            currentNode = currentNode.next;
        }
    }

    @Override
    public void insert(int element, int index) {
        Node elementNode = new Node(element);

        if (index == 0) {
            elementNode.next = first.next;
            first = elementNode;
            count++;
        } else {
            Node node = first;
            node = findNodeByIndexAndNode(index, node);
            if (node == null || node.next == null) {
                System.err.println(String.format("Под данным индексом %s не содержиться элемент", index));
            } else {
                node = elementNode;
                elementNode.next = node.next;
                count++;
            }
        }
    }

    private Node findNodeByIndexAndNode(int index, Node searchNode) {
        for (int i = 0; searchNode != null && i < index - 1; i++) {
            searchNode = searchNode.next;
        }
        return searchNode;
    }

    /**
     * итерируется по элементам списка
     */
    private class LinkedListIterator implements Iterator {

        private Node current = first;

        @Override
        public int next() {
            int value = current.value;
            current = current.next;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node current = this.first;

            while (current != null && i < index) {
                current = current.next;
                i++;
            }

            return current.value;
        }
        System.err.println("Такого элемента нет");
        throw new ArrayIndexOutOfBoundsException();
    }

    @Override
    public int indexOf(int element) {
        int i = 0;
        Node current = this.first;

        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }

        if (current == null) {
            return -1;
        } else {
            return i;
        }
    }

    @Override
    public void removeByIndex(int index) {
        Node node = first;

        if (index == 0) {
            first = node.next;
            count--;
        } else {
            node = findNodeByIndexAndNode(index, node);
            if (node == null || node.next == null) {
                System.err.println(String.format("Под данным индексом %s не содержиться элемент", index));
            } else {
                Node next = node.next.next;
                node.next = next;
                count--;
            }
        }
    }

    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }
}
