package almaz.homework10;

/**
 * Список на основе массива
 */
public class ArrayList implements List {
    /**
     * Константа для начального размера массива
     */
    private static final int DEFAULT_SIZE = 10;

    /**
     * Массив для хранения элементов
     */
    private int data[];

    /**
     * Количество элементов в массиве
     */
    private int count;

    public ArrayList() {
        this.data = new int[DEFAULT_SIZE];
    }

    private class ArrayListIterator implements Iterator {

        private int current = 0;

        @Override
        public int next() {
            int value = data[current];
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }

    @Override
    public int get(int index) {
        if (indexCheck(index)) {
            return this.data[index];
        }
        throw new ArrayIndexOutOfBoundsException();
    }

    @Override
    public int indexOf(int element) {
        for (int i = 0; i < count; i++) {
            if (data[i] == element) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void removeByIndex(int index) {
        if (indexCheck(index)) {
            for (int i = index; i < count - 1; i++) {
                this.data[i] = this.data[i + 1];
            }
            this.count--;
        }
    }

    @Override
    public void add(int element) {
        if (count == data.length - 1) {
            resize();
        }
        data[count] = element;
        count++;
    }

    /**
     * увеличивает массив при переполнении
     */
    private void resize() {
        int oldLength = this.data.length;
        int newLength = oldLength + (oldLength >> 1); //oldLength + oldLength / 2;
        int newData[] = new int[newLength];

        System.arraycopy(this.data, 0, newData, 0, oldLength);

        this.data = newData;
    }

    @Override
    public boolean contains(int element) {
        return indexOf(element) != -1;
    }

    @Override
    public int size() {
        return this.count;
    }

    @Override
    public void removeFirst(int element) {
        if (indexOf(element) != -1) {
            int indexOfRemovingElement = indexOf(element);
            for (int i = indexOfRemovingElement; i < count - 1; i++) {
                this.data[i] = this.data[i + 1];
            }
            this.count--;
        } else {
            System.err.println(String.format("Данный элемент: %s не содержется в списке!", element));
        }
    }

    @Override
    public void insert(int element, int index) {
        if (indexCheck(index)) {
            data[index] = element;
        }
    }

    private boolean indexCheck(int index) {
        if (index > size() && index <= 0) {
            System.err.println(String.format("Индекс превышает размер массива или отрицательный"));
            return false;
        }
        return true;
    }

    @Override
    public Iterator iterator() {
        return new ArrayListIterator();
    }
}
