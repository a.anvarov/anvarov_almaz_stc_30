package almaz.homework4.case2;

/**
 * Фибоначчи с однократным вызовом рекурсии
 */
public class Program {

    /**
     * подсчитывает для заданного n - число ряда Фибоначчи
     *
     * @param args массив аргументов
     */
    public static void main(String[] args) {
        int n = 9;
        System.out.println("Фибоначчи(" + n + ") = " + fibonacci(n));
    }

    /**
     * подсчитывает число ряда Фибоначчи
     *
     * @param sequenceNumber число ряда Фибоначчи
     * @param constantZero   константное число - повтаряется в подсчете
     * @param constantOne    константное число - повтаряется в подсчете
     * @return число ряда Фибоначчи
     */
    private static int fib(int sequenceNumber, int constantZero, int constantOne) {

        if (sequenceNumber == 0) {
            return constantZero;
        }
        if (sequenceNumber == 1) {
            return constantOne;
        }
        return fib(sequenceNumber - 1, constantOne, constantZero + constantOne);
    }

    public static int fibonacci(int sequenceNumber) {
        return fib(sequenceNumber, 0, 1);
    }
}