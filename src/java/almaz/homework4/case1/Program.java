package almaz.homework4.case1;

/**
 * выводит в консоль результат поиска - метод бинарного поиска с помощью рекурсии
 */
public class Program {

    /**
     * запускает алгоритм Бинарного поиска
     *
     * @param args массив аргументов
     */
    public static void main(String[] args) {
        int array[] = {5, 10, 89, 109, 405, 900, 1000, 1001};
        int number = 89;

        if (isSortedArrayAscending(array)) {
            int result = binarySearch(number, array);
            if (result == -1)
                System.out.println("Данный элемент не найден");
            else
                System.out.println("Число " + number + " найдено в массиве, под индексом " + result);
        } else {
            System.out.println("Массив не отсортирован!");
        }
    }

    /**
     * находит элемент алгоритмом Бинарного поиска
     *
     * @param left         левая граница массива
     * @param right        правая граница массива
     * @param searchNumber искомое число
     * @param searchArray  массив для поиска элемента
     * @return индекс элемента массива в котором содержится искомое число, -1 в случе если искомый элемент не найден
     */
    private static int binarySearchRecursion(int left, int right, int searchNumber, int searchArray[]) {

        if (right >= left) {
            int middle = left + (right - 1) / 2;

            if (searchArray[middle] == searchNumber) {
                return middle;
            }

            if (searchArray[middle] > searchNumber) {
                return binarySearchRecursion(left, middle - 1, searchNumber, searchArray);
            } else {
                return binarySearchRecursion(middle + 1, right, searchNumber, searchArray);
            }
        }

        return -1;
    }

    /**
     * вызывает рекрсивный метод Бинарного поиска
     *
     * @param searchNumber искомое число
     * @param searchArray  массив для поиска элемента
     * @return индекс элемента массива в котором содержится искомое число, -1 в случе если искомый элемент не найден
     */
    public static int binarySearch(int searchNumber, int searchArray[]) {
        return binarySearchRecursion(0, searchArray.length - 1, searchNumber, searchArray);
    }

    /**
     * проверяет сортированность массива
     *
     * @param array массив который нужно проверить
     * @return true - если массив отсортирован, false - в противном случае
     */
    public static boolean isSortedArrayAscending(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] > array[i + 1]) {
                return false;
            }
        }
        return true;
    }
}