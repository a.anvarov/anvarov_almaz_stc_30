package almaz.homework13.dao;

import almaz.homework13.mappers.Mapper;
import almaz.homework13.models.Course;
import almaz.homework13.utils.ToStringFunction;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Реализация интерфейса Курс Дао, с помощью Потоковой записи в файл
 */
public class CourseDaoFileOutputStreamImpl implements CourseDao {
    // TODO  не вижу код по сохранению списков

    private String fileName;

    /**
     * Для преобразованя модели данных Курса в строку
     */
    private ToStringFunction toStringFunction;

    /**
     * Для преобразования из строки в объект
     */
    private Mapper<String, Course> stringToCourseMapper = string -> {
        String data[] = string.split(" "); // TODO если строка не соответсвует формату, то будет вылетать NullPointer
        return new Course(Integer.parseInt(data[0]), data[1], format.parse(data[2]), format.parse(data[3]));
    };

    /**
     * Преобразует дату в троку или, форматирует дату в строку
     */
    private static DateFormat format = new SimpleDateFormat("dd.MM.yyyy");


    public CourseDaoFileOutputStreamImpl(String fileName, ToStringFunction toStringFunction) {
        this.fileName = fileName;
        this.toStringFunction = toStringFunction;
    }

    @Override
    public Course find(int id) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));

            String current = bufferedReader.readLine();

            while (current != null) {
                Course courseFromCurrent = stringToCourseMapper.map(current);
                if (courseFromCurrent.getId() == id) {
                    bufferedReader.close(); // TODO почему потом закрывается только если найдена соответсвующая запись?
                    return courseFromCurrent;
                }
                current = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e); // TODO не корерктно тут брость IllegalArgumentException, потому что в данном случае ошибки ввода вывода не зависят от пераднных параметров
        } catch (ParseException pe) {
            throw new IllegalArgumentException(pe); // TODO аналогично комменту выше
        }
        return null;
    }

    @Override
    public void save(Course course) {
        try {
            OutputStream outputStream = new FileOutputStream(fileName, true);
            outputStream.write(toStringFunction.toString(course).getBytes());
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e); // TODO аналогично комменту выше
        } catch (IOException e) {
            throw new IllegalStateException(e); // TODO аналогично комменту выше
        }
    }
}
