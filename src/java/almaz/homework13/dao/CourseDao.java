package almaz.homework13.dao;

import almaz.homework13.models.Course;

/**
 * Дао класс для модели "Курс"
 */
public interface CourseDao {
    Course find(int id);

    void save(Course course);
}
