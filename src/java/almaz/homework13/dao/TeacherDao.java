package almaz.homework13.dao;

import almaz.homework13.models.Teacher;

/**
 * Дао класс для модели "Преподаватель"
 */
public interface TeacherDao {
    Teacher find(int id);

    void save(Teacher teacher);
}
