package almaz.homework13.dao;

import almaz.homework13.mappers.Mapper;
import almaz.homework13.models.Teacher;
import almaz.homework13.utils.ToStringFunction;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Реализация интерфейса Преподаватель Дао, с помощью Потоковой записи в файл
 */
public class TeacherDaoFileOutputStreamImpl implements TeacherDao {

    /**
     * Имя файла
     */
    private String fileName;
    /**
     * Для преобразованя модели данных Преподаватель в строку
     */
    private ToStringFunction<Teacher> teacherToStringFunction;

    /**
     * Для преобразования из строки в объект
     */
    private Mapper<String, Teacher> stringToTeacherMapper = string -> {
        String data[] = string.split(" ");
        return new Teacher(Integer.parseInt(data[0]), data[1], data[2], Integer.parseInt(data[3]));
    };

    /**
     * преобразует дату в троку или, форматирует дату в строку
     */
    private static DateFormat format = new SimpleDateFormat("dd.MM.yyyy");

    public TeacherDaoFileOutputStreamImpl(String fileName, ToStringFunction teacherToStringFunction) {
        this.fileName = fileName;
        this.teacherToStringFunction = teacherToStringFunction;
    }

    @Override
    public Teacher find(int id) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));

            String current = bufferedReader.readLine();

            while (current != null) {
                Teacher teacherFromCurrent = stringToTeacherMapper.map(current);
                if (teacherFromCurrent.getId() == id) {
                    bufferedReader.close();
                    return teacherFromCurrent;
                }
                current = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } catch (ParseException pe) {
            throw new IllegalArgumentException(pe);
        }
        return null;
    }

    @Override
    public void save(Teacher teacher) {
        try {
            OutputStream outputStream = new FileOutputStream(fileName, true);
            outputStream.write(teacherToStringFunction.toString(teacher).getBytes());
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
