package almaz.homework13.dao;

import almaz.homework13.mappers.Mapper;
import almaz.homework13.models.Lesson;
import almaz.homework13.utils.ToStringFunction;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Реализация интерфейса Урок Дао, с помощью Потоковой записи в файл
 */
public class LessonDaoFileOutputStreamImpl implements LessonDao {

    private String fileName;
    /**
     * Для преобразованя модели данных Курса в строку
     */
    private ToStringFunction<Lesson> lessonToStringFunction;

    /**
     * Для преобразования из строки в объект
     */
    private Mapper<String, Lesson> stringToLessonMapper = string -> {
        String data[] = string.split(" ");
        return new Lesson(Integer.parseInt(data[0]), format.parse(data[1]), data[2]);
    };

    /**
     * преобразует дату в троку или, форматирует дату в строку
     */
    private static DateFormat format = new SimpleDateFormat("dd.MM.yyyy");


    public LessonDaoFileOutputStreamImpl(String fileName, ToStringFunction lessonToStringFunction) {
        this.fileName = fileName;
        this.lessonToStringFunction = lessonToStringFunction;
    }

    @Override
    public Lesson find(int id) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));

            String current = bufferedReader.readLine();

            while (current != null) {
                Lesson lessonFromCurrent = stringToLessonMapper.map(current);
                if (lessonFromCurrent.getId() == id) {
                    bufferedReader.close();
                    return lessonFromCurrent;
                }
                current = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } catch (ParseException pe) {
            throw new IllegalArgumentException(pe);
        }
        return null;
    }

    @Override
    public void save(Lesson lesson) {
        try {
            OutputStream outputStream = new FileOutputStream(fileName, true);
            outputStream.write(lessonToStringFunction.toString(lesson).getBytes());
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
