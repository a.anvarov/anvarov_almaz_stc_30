package almaz.homework13.dao;

import almaz.homework13.models.Lesson;

/**
 * Дао класс для модели "Урок"
 */
public interface LessonDao {
    Lesson find(int id);

    void save(Lesson lesson);
}
