package almaz.homework13.mappers;

import java.text.ParseException;

public interface Mapper<T, W> {
    W map(T t) throws ParseException;
}
