package almaz.homework13.example;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {


    public static void main(String[] args) throws IOException {

        Path path = Paths.get("D:\\DEV\\vladislav_yakovlev_stc_30\\java\\java\\Vladislav\\homework13\\src\\example\\base.txt");
        if (!Files.exists(path)) {
            Files.createFile(path);
        }

        User user = new User(1, "Vladislav", "123", Arrays.asList("first", "second", "third"));

        DaoUser daoUser = new DaoUser(new Mapper(), path);
        DaoPost daoPost = new DaoPost();

        daoUser.save(user);
        List<User> all = daoUser.findAll();
        System.out.println(all);


        User user2 = daoUser.findById(1);
        user2.getPosts().get(0);

        daoPost.findPost(user2.getPosts().get(0));

        List<Integer> userIds = new ArrayList<>();
        List<User> users = daoUser.findAll();


    }
}
