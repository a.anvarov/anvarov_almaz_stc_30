package almaz.homework13.example;

import java.util.Arrays;

public class Mapper {

    public String map(User user) {
        return user.getId() + " " + user.getName() + " " + user.getPassword() + " " + String.join(";", user.getPosts());
    }
    public User map(String user) {

        if (user == null) {
            throw new IllegalArgumentException();
        }
        String[] array = user.split(" ");
        if (array.length != User.class.getDeclaredFields().length){
            throw new IllegalArgumentException("");
        }
        return new User(Integer.parseInt(array[0]), array[1], array[2], Arrays.asList(array[3].split(";")));
    }
}
