package almaz.homework13.example;

import java.io.*;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class DaoUser {

    private Mapper mapper;
    private Path source;

    public DaoUser(Mapper mapper, Path source) {
        this.mapper = mapper;
        this.source = source;
    }

    public boolean save(User user) throws IOException {
        try (PrintStream writer = new PrintStream(new FileOutputStream(source.toFile(), true))) {
            if (findAll().stream().noneMatch(line -> user.getId() == line.getId())){
                writer.println(mapper.map(user));
                return true;
            } else {
                return false;
            }
        }
    }

    public boolean save(List<User> users) throws IOException {
        return save(users, true);
    }

    public boolean save(List<User> users, boolean append) throws IOException {
        try (PrintStream writer = new PrintStream(new FileOutputStream(source.toFile(), append))) {
            users.forEach(user -> writer.println(mapper.map(user)));
        }
        return true;
    }

    public List<User> findAll() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(source.toFile()))) {
//            ArrayList<User> users = new ArrayList<>();
//            String line = reader.readLine();
//            while (line != null && !line.equals("")) {
//                users.add(mapper.map(line));
//            }
            return reader.lines()
                    .filter(Objects::nonNull)
                    .map(mapper::map)
                    .collect(Collectors.toList());
        }
    }

    public User findById(int id) throws IOException {
        return findAll().stream()
                .filter(user -> user.getId() == id)
                .findFirst().get();
    }

    public User findByName(String name) throws IOException {
        return findAll().stream()
                .filter(user -> user.getName().equals(name))
                .findFirst().get();
    }

    public boolean deleteById(int id) throws IOException {
        List<User> all = findAll();
        boolean wasDeleted = all.removeIf(user -> user.getId() == id);
        save(all, false);
        return wasDeleted;
    }


}
