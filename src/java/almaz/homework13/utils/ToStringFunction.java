package almaz.homework13.utils;

public interface ToStringFunction<T> {
    String toString(T t);
}
