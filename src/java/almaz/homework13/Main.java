package almaz.homework13;

import almaz.homework13.dao.*;
import almaz.homework13.models.Course;
import almaz.homework13.models.Lesson;
import almaz.homework13.models.Teacher;
import almaz.homework13.service.ServiceCourseLessonImpl;
import almaz.homework13.service.ServiceCourseTeacherImpl;
import almaz.homework13.utils.ToStringFunction;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

/**
 * Для проверки работы "Образовательного центра" - для модели Курс
 */
public class Main {

    /**
     * Путь к файлу для Курсов
     */
    private static final String COURSE_PATH = "src/java/almaz/homework13/files/course.txt";

    /**
     * Путь к файлу для Уроки
     */
    private static final String LESSONS_PATH = "src/java/almaz/homework13/files/lesson.txt";

    /**
     * Путь к файлу для Преподвателей
     */
    private static final String TEACHER_PATH = "src/java/almaz/homework13/files/teacher.txt";

    /**
     * Путь к файлу для Курсов - Преподвателей
     */
    private static final String COURSE_TEACHER_PATH = "src/java/almaz/homework13/files/course_teacher.txt";

    /**
     * Путь к файлу для Курсов - Уроков
     */
    private static final String COURSE_LESSON_PATH = "src/java/almaz/homework13/files/course_lesson.txt";

    /**
     * преобразует дату в троку или, форматирует дату в строку
     */
    private static DateFormat format = new SimpleDateFormat("dd.MM.yyyy");

    /**
     * ДАО класс для работы с курсами
     */
    private static CourseDao courseDao;

    /**
     * ДАО класс для работы с уроками
     */
    private static LessonDao lessonDao;

    /**
     * ДАО класс для работы с преподавателями
     */
    private static TeacherDao teacherDao;

    /**
     * Сервисный класс для сохраненния смежных таблиц
     */
    private static ServiceCourseLessonImpl serviceCourseLesson;

    /**
     * Сервисный класс для сохраненния смежных таблиц
     */
    private static ServiceCourseTeacherImpl serviceCourseTeacher;

    public static void main(String[] args) throws ParseException {

        Course course1 = new Course(1, "Python", format.parse("29.11.2020"), format.parse("29.01.2021"));
        Course course2 = new Course(2, "Ruby", format.parse("01.12.2020"), format.parse("01.03.2021"));
        Course course3 = new Course(3, "Java", format.parse("01.12.2020"), format.parse("01.03.2021"));
        Course course4 = new Course(4, "Javascript", format.parse("01.12.2020"), format.parse("01.03.2021"));
        Course course5 = new Course(5, "Perl", format.parse("01.12.2020"), format.parse("01.03.2021"));
        Course course6 = new Course(6, "Php", format.parse("01.12.2020"), format.parse("01.03.2021"));

        Teacher teacher1 = new Teacher(1, "Bucky", "Roberts", 10);
        teacher1.setCourses(Arrays.asList(course1.getId(), course2.getId()));
        Teacher teacher2 = new Teacher(2, "Bruce", "Eckel", 20);
        teacher2.setCourses(Arrays.asList(course3.getId(), course4.getId()));
        Teacher teacher3 = new Teacher(3, "Herbert", "Schildt", 15);
        teacher3.setCourses(Arrays.asList(course5.getId(), course6.getId()));

        Lesson lesson1 = new Lesson(1, format.parse("01.12.2020"), "Python Basics");
        lesson1.setCourse(course1.getId());

        Lesson lesson2 = new Lesson(2, format.parse("01.12.2020"), "Ruby Basics");
        lesson2.setCourse(course2.getId());

        Lesson lesson3 = new Lesson(3, format.parse("01.12.2020"), "Java Basics");
        lesson3.setCourse(course3.getId());

        Lesson lesson4 = new Lesson(4, format.parse("01.12.2020"), "Javascript Basics");
        lesson4.setCourse(course4.getId());

        Lesson lesson5 = new Lesson(5, format.parse("01.12.2020"), "Perl Basics");
        lesson5.setCourse(course5.getId());

        Lesson lesson6 = new Lesson(6, format.parse("01.12.2020"), "Php Basics");
        lesson6.setCourse(course6.getId());

        ToStringFunction<Course> courseToStringFunction = course -> // TODO эти функции должны быть описаны в DAO
                course.getId() + " "
                        + course.getCourseName() + " "
                        + format.format(course.getStartDate()) + " "
                        + format.format(course.getEndDate()) + "\n";

        ToStringFunction<Lesson> lessonToStringFunction = lesson ->
                lesson.getId() + " "
                        + lesson.getName() + " "
                        + format.format(lesson.getDateTimeLesson()) + "\n";

        ToStringFunction<Teacher> teacherToStringFunction = teacher ->
                teacher.getId() + " "
                        + teacher.getName() + " "
                        + teacher.getSecondName() + " "
                        + teacher.getWorkExperience() + "\n";

        courseDao = new CourseDaoFileOutputStreamImpl(COURSE_PATH, courseToStringFunction);

        lessonDao = new LessonDaoFileOutputStreamImpl(LESSONS_PATH, lessonToStringFunction);

        teacherDao = new TeacherDaoFileOutputStreamImpl(TEACHER_PATH, teacherToStringFunction);

        // Сохранение данных по Курсам
        courseDao.save(course1);
        courseDao.save(course2);
        courseDao.save(course3);
        courseDao.save(course4);
        courseDao.save(course5);
        courseDao.save(course6);

        // Сохранение данных по Урокам
        lessonDao.save(lesson1);
        lessonDao.save(lesson2);
        lessonDao.save(lesson3);
        lessonDao.save(lesson4);
        lessonDao.save(lesson5);
        lessonDao.save(lesson6);

        // Сохранение данных по Преподавателям
        teacherDao.save(teacher1);
        teacherDao.save(teacher2);
        teacherDao.save(teacher3);

        serviceCourseLesson = new ServiceCourseLessonImpl(COURSE_LESSON_PATH);
        serviceCourseTeacher = new ServiceCourseTeacherImpl(COURSE_TEACHER_PATH);

        for (Integer course_id: teacher1.getCourses()) {
            Course course = courseDao.find(course_id);
            serviceCourseTeacher.save(course, teacher1);
        }

        for (Integer course_id: teacher2.getCourses()) {
            Course course = courseDao.find(course_id);
            serviceCourseTeacher.save(course, teacher2);
        }

        for (Integer course_id: teacher3.getCourses()) {
            Course course = courseDao.find(course_id);
            serviceCourseTeacher.save(course, teacher3);
        }

        serviceCourseLesson.save(lesson1, course1);
        serviceCourseLesson.save(lesson2, course2);
        serviceCourseLesson.save(lesson3, course3);
        serviceCourseLesson.save(lesson4, course4);
        serviceCourseLesson.save(lesson5, course5);
        serviceCourseLesson.save(lesson6, course6);

        System.out.println();
    }
}
