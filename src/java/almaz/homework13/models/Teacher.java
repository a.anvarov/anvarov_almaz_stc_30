package almaz.homework13.models;

import java.util.List;

/**
 * Модель данных "Преподаватель"
 */
public class Teacher {
    private int id;
    private String name;
    private String secondName;
    private int workExperience;

    List<Integer> courses;

    public Teacher(int id, String name, String secondName, int workExperience) {
        this.id = id;
        this.name = name;
        this.secondName = secondName;
        this.workExperience = workExperience;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public int getWorkExperience() {
        return workExperience;
    }

    public void setWorkExperience(int workExperience) {
        this.workExperience = workExperience;
    }

    public List<Integer> getCourses() {
        return courses;
    }

    public void setCourses(List<Integer> courses) {
        this.courses = courses;
    }
}
