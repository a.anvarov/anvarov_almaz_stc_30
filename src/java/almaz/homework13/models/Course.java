package almaz.homework13.models;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Модель данных "Курс"
 */
public class Course {
    private int id;
    private String courseName;
    private Date startDate;
    private Date endDate;

    private List<Long> teachers;
    private List<Long> lessons;

    public Course(int id, String courseName, Date startDate, Date endDate) {
        this.id = id;
        this.courseName = courseName;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Course(int id, String courseName, Date startDate, Date endDate, List<Long> teachers, List<Long> lessons) {
        this.id = id;
        this.courseName = courseName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.teachers = teachers;
        this.lessons = lessons;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<Long> getTeachers() {
        return teachers;
    }

    public void setTeachers(List<Long> teachers) {
        this.teachers = teachers;
    }

    public List<Long> getLessons() {
        return lessons;
    }

    public void setLessons(List<Long> lessons) {
        this.lessons = lessons;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return id == course.id &&
                Objects.equals(courseName, course.courseName) &&
                Objects.equals(startDate, course.startDate) &&
                Objects.equals(endDate, course.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, courseName, startDate, endDate);
    }

}
