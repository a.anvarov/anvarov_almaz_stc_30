package almaz.homework13.models;

import java.util.Date;
import java.util.Objects;

/**
 * Модель данных "Урок"
 */
public class Lesson {
    private int id;
    private Date dateTimeLesson;
    private String name;

    private Integer course;

    public Lesson(int id, Date dateTimeLesson, String name, Integer course) {
        this.id = id;
        this.dateTimeLesson = dateTimeLesson;
        this.name = name;
        this.course = course;
    }

    public Lesson(int id, Date dateTimeLesson, String name) {
        this.id = id;
        this.dateTimeLesson = dateTimeLesson;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateTimeLesson() {
        return dateTimeLesson;
    }

    public void setDateTimeLesson(Date dateTimeLesson) {
        this.dateTimeLesson = dateTimeLesson;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lesson lesson = (Lesson) o;
        return Objects.equals(dateTimeLesson, lesson.dateTimeLesson) &&
                Objects.equals(name, lesson.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dateTimeLesson, name);
    }

}
