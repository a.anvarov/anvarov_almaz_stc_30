package almaz.homework13.service;

import almaz.homework13.models.Course;
import almaz.homework13.models.Teacher;

public interface ServiceCourseTeacher {
    void save(Course course, Teacher teacher);
}
