package almaz.homework13.service;

import almaz.homework13.models.Course;
import almaz.homework13.models.Lesson;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class ServiceCourseLessonImpl implements ServiceCourseLesson {

    private String fileName;

    public ServiceCourseLessonImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void save(Lesson lesson, Course course) {
        try {
            OutputStream outputStream = new FileOutputStream(fileName, true); // TODO здесь можно (нужно) использовать DAO
            outputStream.write((lesson.getId() + " " + course.getId() + "\n").getBytes());
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
