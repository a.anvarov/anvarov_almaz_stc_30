package almaz.homework13.service;

import almaz.homework13.models.Course;
import almaz.homework13.models.Teacher;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class ServiceCourseTeacherImpl implements ServiceCourseTeacher {

    String fileName;

    public ServiceCourseTeacherImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void save(Course course, Teacher teacher){
        try {
            OutputStream outputStream = new FileOutputStream(fileName, true);
            outputStream.write((course.getId() + " " + teacher.getId() + "\n").getBytes());
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
