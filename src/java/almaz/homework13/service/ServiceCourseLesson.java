package almaz.homework13.service;

import almaz.homework13.models.Course;
import almaz.homework13.models.Lesson;
import almaz.homework13.models.Teacher;

public interface ServiceCourseLesson {
    void save(Lesson lesson, Course course);
}
