package almaz.interfaces;

import java.util.ArrayList;
import java.util.Arrays;

public class ModernITCompany extends Company implements AbstractITCompany {
    private ArrayList<SoftMaker> softMakers = new ArrayList<>();

    public ModernITCompany(SoftMaker... SoftMakers) {
        this.softMakers.addAll(Arrays.asList(SoftMakers.clone()));
    }

    public void addEmploy(SoftMaker SoftMaker) {
        softMakers.add(SoftMaker);
    }

    @Override
    public void makeProgram() {
        printState();
        for (SoftMaker SoftMaker : softMakers) {
            SoftMaker.makeCode();
        }
    }

    void printState() {
        System.out.println("modern company's employs");
        System.out.println("I have " + getNetsCount() + " neyroNets in state");
        System.out.println("I have " + (softMakers.size() - getNetsCount()) + " other employs");
    }

    private int getNetsCount(){
        int count = 0;
        for (SoftMaker softMaker : softMakers) {
            if (softMaker instanceof NeyroNet) {
                count++;
            }
        }
        return count;
    }
}
