package almaz.interfaces;

public class Human {

    private String name;

    public Human(String name) {
        this.name = name;
    }

    public void eat(){
        sayWhatIDo("I am eating");
    }

    protected void sayWhatIDo(String action) {
        System.out.println(getName() + ": " + action);
    }

    public String getName() {
        return name;
    }
}
