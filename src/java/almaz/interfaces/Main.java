package almaz.interfaces;

public class Main {

    public static void main(String[] args) {
        Programmer misha = new Programmer("Misha", Special.BACK);
        Programmer masha = new Programmer("Masha", Special.FRONT);
        Programmer dima = new Programmer("Dima", Special.BIG_DATA);

        NeyroNet neyroNet1 = new NeyroNet();
        NeyroNet neyroNet2 = new NeyroNet();

        ITCompany itCompany = new ITCompany(misha, masha, dima);
        NeyroTCompany neyroTCompany = new NeyroTCompany(neyroNet1, neyroNet2);
        ModernITCompany modernITCompany = new ModernITCompany(dima, neyroNet2);

        Orderer orderer = new Orderer();
        orderer.orderProgram(itCompany);
        orderer.orderProgram(modernITCompany);
        orderer.orderProgram(neyroTCompany);
    }
}
