package almaz.interfaces;

import java.util.ArrayList;
import java.util.Arrays;

public class NeyroTCompany extends Company implements AbstractITCompany {
    private ArrayList<NeyroNet> neyroNets = new ArrayList<>();

    public NeyroTCompany(NeyroNet... NeyroNets) {
        this.neyroNets.addAll(Arrays.asList(NeyroNets.clone()));
    }

    public void addEmploy(NeyroNet NeyroNet) {
        neyroNets.add(NeyroNet);
    }

    @Override
    public void makeProgram() {
        printState();
        for (NeyroNet NeyroNet : neyroNets) {
            NeyroNet.makeCode();
        }
    }

    public void checkPhotos() {
        printState();
        for (NeyroNet NeyroNet : neyroNets) {
            NeyroNet.checkPhoto();
        }
    }

    void printState() {
        System.out.println("modern company's employs");
        System.out.println("I have " + neyroNets.size() + " neyroNets in state");
        
    }
}
