package almaz.interfaces;

public enum Special {

    FRONT("frontend"),
    BACK("backand"),
    BIG_DATA("data sciencer");

    private String description;


    Special(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return getDescription();
    }
}
