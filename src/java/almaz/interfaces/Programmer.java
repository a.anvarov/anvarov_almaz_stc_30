package almaz.interfaces;

public class Programmer extends Human implements SoftMaker{

    private Special special;

    public Programmer(String name, Special special) {
        super(name);
        this.special = special;
    }

    public Programmer(String name) {
        super(name);
    }

    public void work() {
        makeCode();
        eat();
    }

    @Override
    public void makeCode() {
        drinkCoffee();
        sayWhatIDo("I am making code");
    }

    void drinkCoffee() {
        sayWhatIDo("I am drinking coffee");
    }

    void saySpecial() {
        if (special == null) {
            sayWhatIDo("I don't have special");
        } else {
            sayWhatIDo(String.format("I have special: %s", special));
        }
    }

    public Special getSpecial() {
        return special;
    }
}
