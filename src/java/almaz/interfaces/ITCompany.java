package almaz.interfaces;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 */
public class ITCompany extends Company implements AbstractITCompany{

    private ArrayList<Programmer> programmers = new ArrayList<>();

    public ITCompany(Programmer... programmers) {
        this.programmers.addAll(Arrays.asList(programmers.clone()));
    }

    public void addEmploy(Programmer programmer) {
        programmers.add(programmer);
    }

    @Override
    public void makeProgram() {
        printState();
        for (Programmer programmer : programmers) {
            programmer.work();
        }
    }

    public void printState() {
        System.out.println("company's employs");
        for (Programmer programmer : programmers) {
            System.out.printf("\nSpecial: %s - EmployName = %s", programmer.getSpecial(), programmer.getName());
        }
    }

}
