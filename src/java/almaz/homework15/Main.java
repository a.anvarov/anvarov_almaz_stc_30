package almaz.homework15;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static java.nio.file.Files.lines;

public class Main {

    private static final Path path = Paths.get("src/java/almaz/homework15/text.txt");

    public static void main(String[] args) {
        try {
            List<String> words = lines(path).collect(Collectors.toList());

            QuickSort quickSort = new QuickSort();

            List<String> sortedList = quickSort.sort(words);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
