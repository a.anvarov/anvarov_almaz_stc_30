package almaz.homework3.case3;

import java.util.Arrays;
import java.util.Scanner;

/**
 * программа ДЗ №3 - Занятие 03 - Подпрограммы - назначение, использование. Указатели, адреса, передача аргументов в параметры
 * дополнительное задание
 */
class Program {

    /**
     * метод запускатор с параметрами
     * Запуская программу с консоли, не забывайте передавать параметры!
     * пример: java Program3 sum 5 0 1 2 3 4
     * Список доступных операций: sum, revert, avg, swap, sort, join
     *
     * @param args массив аргументов
     */
    public static void main(String[] args) {

        if (args.length > 2) {
            String operation = args[0];

            int arrayLength = args.length - 1;

            int[] array = new int[arrayLength];

            int index = 0;
            for (int i = 1; i < args.length; i++) {
                array[index] = Integer.parseInt(args[i]);
                index++;
            }

            switch (operation) {
                case "sum":
                    int summary = arrayElementsSummary(array);
                    System.out.println(summary);
                    break;
                case "revert":
                    int[] reversedArray = arrayElementsReverse();
                    System.out.println(Arrays.toString(reversedArray));
                    break;
                case "avg":
                    double average = arrayElementsAverage();
                    System.out.println(average);
                    break;
                case "swap":
                    int[] swappedArray = swapMaximalAndMinimalArrayElements(array);
                    System.out.print(Arrays.toString(swappedArray));
                    break;
                case "sort":
                    int[] sortedArray = bubbleSort(array);
                    System.out.println(Arrays.toString(sortedArray));
                    break;
                case "join":
                    int joinedElementsNumber = arrayToNumber(array);
                    System.out.println(joinedElementsNumber);
                    break;
                default:
                    System.out.println("Не найдена введенная операция " + "'" + args[0] + "'");
                    System.out.println("Список доступных операций: ");
                    System.out.println("sum, revert, avg, swap, sort, join");
            }
        } else {
            System.out.println("Запуская программу с консоли, не забывайте передавать параметры!");
            System.out.println("пример: java Program3 sum 5 0 1 2 3 4");
            System.out.println("Кроме функций avg, revert - для них данные вводятся с консоли");
            System.out.println("пример: java Program3 avg");
            System.out.println("пример: java Program3 revert");
        }
    }

    /**
     * подсчитывает сумму элементов массива
     *
     * @param array массив числовой
     * @return сумма элементов
     */
    public static int arrayElementsSummary(int[] array) {
        int elementsSummary = 0;

        for (int i = 0; i < array.length; i++) {
            elementsSummary += array[i];
        }

        return elementsSummary;
    }

    /**
     * разворачивает в обратном порядке числа в массиве
     *
     * @return массив чисел
     */
    public static int[] arrayElementsReverse() {
        int[] array = fillArrayFromConsole();

        int temporaryVariable;
        for (int i = 0; i < (array.length / 2); ++i) {
            temporaryVariable = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = temporaryVariable;
        }

        return array;
    }

    /**
     * подсчитывает среднее арифметическое элементов в массиве
     *
     * @return среднее арифметическое
     */
    public static double arrayElementsAverage() {
        int[] array = fillArrayFromConsole();

        double arrayAverage;

        int arraySum = 0;
        for (int i = 0; i < array.length; i++) {
            arraySum += array[i];
        }

        arrayAverage = arraySum * 1.0 / array.length;

        return arrayAverage;
    }

    /**
     * меняет местами максимальный и минимальный элементы в массиве
     *
     * @param array числовой массив
     * @return массив чисел
     */
    public static int[] swapMaximalAndMinimalArrayElements(int[] array) {
        int maximalElement = array[0];
        int minimalElement = array[0];

        int maximalElementIndex = 0;
        int minimalElementIndex = 0;

        for (int i = 0; i < array.length; i++) {
            if (maximalElement >= array[i]) {
                maximalElement = array[i];
                maximalElementIndex = i;
            }

            if (minimalElement <= array.length) {
                minimalElement = array[i];
                minimalElementIndex = i;
            }
        }

        int temporary = array[maximalElementIndex];
        array[maximalElementIndex] = array[minimalElementIndex];
        array[minimalElementIndex] = temporary;

        return array;
    }

    /**
     * сортирует массив чисел методом Пузырька
     *
     * @param array массив чисел
     * @return отсортированный массив чисел
     */
    public static int[] bubbleSort(int[] array) {

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int swapVariable = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = swapVariable;
                }
            }
        }
        return array;
    }

    /**
     * из элементов массива получает число
     *
     * @param array массив чисел
     * @return целое число
     */
    public static int arrayToNumber(int[] array) {
        int number = 0;

        for (int i = 0; i < array.length; i++) {
            number *= 10;
            number += array[i];
        }

        return number;
    }

    /**
     * ввод массива через клавиатуру
     *
     * @return массив чисел введенный с клавиатуры
     */
    private static int[] fillArrayFromConsole() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите размер массива: ");

        int arrayLength = scanner.nextInt();
        int array[] = new int[arrayLength];

        System.out.println("Введите элементы массива через 'enter': ");

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        return array;
    }
}
