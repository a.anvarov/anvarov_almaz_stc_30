package almaz.homework3.case2;

/**
 * программа расчитывающая определенный интеграл методом Симпсона
 */
class Program {

    /**
     * метод который запускает подсчет определенного интеграла методом Симпсона
     *
     * @param args массив данных при запуске программы
     */
    public static void main(String[] args) {
        int splitNumbers[] = {10, 100, 1_000, 10_000, 100_000, 1000_000};

        printIntegralResultsForN(0, 10, splitNumbers);
    }

    /**
     * выводит в консоль результат определенного интеграла для заданного N
     *
     * @param firstPoint   начальная точка отрезка
     * @param secondPoint  конечная точка отрезка
     * @param splitNumbers массив чисел N - разбиение отрезка
     */
    public static void printIntegralResultsForN(double firstPoint, double secondPoint, int splitNumbers[]) {
        for (int i = 0; i < splitNumbers.length; i++) {
            System.out.print("For N = ");
            System.out.printf("%10s", +splitNumbers[i]);
            System.out.println(", result = " + integralBySimpson(firstPoint, secondPoint, splitNumbers[i]));
        }
    }

    /**
     * подсчитывает определенный интеграл методом Симпсона
     *
     * @param firstPoint  начальная точка отрезка
     * @param secondPoint конечная точка отрезка
     * @param number      число разбиения отрезка
     * @return вычисленное значение определенного интеграла
     */
    public static double integralBySimpson(double firstPoint, double secondPoint, int number) {
        double height = (secondPoint - firstPoint) / number;

        double result = 0;
        double step = firstPoint + height;

        while (step < secondPoint) {
            result = result + 4 * mathFunction(step);
            step = step + height;
            result = result + 2 * mathFunction(step);
            step = step + height;
        }

        result = (height / 3) * (result + mathFunction(firstPoint) - mathFunction(secondPoint));

        return result;
    }

    /**
     * подсчитывает значение математической функции
     *
     * @param x значение шага разбиения
     * @return значение функции от x
     */
    public static double mathFunction(double x) {
        return Math.sin(x);
    }
}