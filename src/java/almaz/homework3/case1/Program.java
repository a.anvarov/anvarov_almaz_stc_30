package almaz.homework3.case1;

import java.util.Scanner;

/**
 * программа ДЗ №3 - Занятие 03 - Подпрограммы - назначение, использование. Указатели, адреса, передача аргументов в параметры
 */
class Program {

    public static void main(String[] args) {

    }

    /**
     * подсчитывает сумму элементов массива
     *
     * @param array массив числовой
     * @return сумма элементов
     */
    public static int arrayElementsSummary(int[] array) {
        int elementsSummary = 0;

        for (int i = 0; i < array.length; i++) {
            elementsSummary += array[i];
        }

        return elementsSummary;
    }

    /**
     * разворачивает в обратном порядке числа в массиве
     *
     * @return массив чисел
     */
    public static int[] arrayElementsReverse() {
        int[] array = fillArrayFromConsole();

        int temporaryVariable;
        for (int i = 0; i < (array.length / 2); ++i) {
            temporaryVariable = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = temporaryVariable;
        }

        return array;
    }

    /**
     * подсчитывает среднее арифметическое элементов в массиве
     *
     * @return среднее арифметическое
     */
    public static double arrayElementsAverage() {
        int[] array = fillArrayFromConsole();

        double arrayAverage;

        int arraySum = 0;
        for (int i = 0; i < array.length; i++) {
            arraySum += array[i];
        }

        arrayAverage = arraySum * 1.0 / array.length;

        return arrayAverage;
    }

    /**
     * меняет местами максимальный и минимальный элементы в массиве
     *
     * @param array числовой массив
     * @return массив чисел
     */
    public static int[] swapMaximalAndMinimalArrayElements(int[] array) {
        int maximalElement = array[0];
        int minimalElement = array[0];

        int maximalElementIndex = 0;
        int minimalElementIndex = 0;

        for (int i = 0; i < array.length; i++) {
            if (maximalElement >= array[i]) {
                maximalElement = array[i];
                maximalElementIndex = i;
            }

            if (minimalElement <= array.length) {
                minimalElement = array[i];
                minimalElementIndex = i;
            }
        }

        int temporary = array[maximalElementIndex];
        array[maximalElementIndex] = array[minimalElementIndex];
        array[minimalElementIndex] = temporary;

        return array;
    }

    /**
     * сортирует массив чисел методом Пузырька
     *
     * @param array массив чисел
     * @return отсортированный массив чисел
     */
    public static int[] bubbleSort(int[] array) {

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int swapVariable = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = swapVariable;
                }
            }
        }
        return array;
    }

    /**
     * из элементов массива получает число
     *
     * @param array массив чисел
     * @return целое число
     */
    public static int arrayToNumber(int[] array) {
        int number = 0;

        for (int i = 0; i < array.length; i++) {
            number *= 10;
            number += array[i];
        }

        return number;
    }

    /**
     * ввод массива через клавиатуру
     *
     * @return массив чисел введенный с клавиатуры
     */
    private static int[] fillArrayFromConsole() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите размер массива: ");

        int arrayLength = scanner.nextInt();
        int array[] = new int[arrayLength];

        System.out.println("Введите элементы массива через 'enter': ");

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        return array;
    }
}