package almaz.homework7;

/**
 * демонстрирует работу шаблона Builder
 */
public class User {

    private String firstName;
    private String lastName;
    private int age;
    private boolean worker;

    private User() {
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public boolean isWorker() {
        return worker;
    }

    public static Builder builder() {
        return new User().new Builder();
    }

    /**
     * для инициализации полей класса User
     */
    public class Builder {

        private Builder() {
        }

        public Builder firstName(String firstName) {
            User.this.firstName = firstName;

            return this;
        }

        public Builder lastName(String lastName) {
            User.this.lastName = lastName;

            return this;
        }

        public Builder age(int age) {
            User.this.age = age;

            return this;
        }

        public Builder isWorker(boolean worker) {
            User.this.worker = worker;

            return this;
        }

        public User build() {
            return User.this;
        }
    }
}
