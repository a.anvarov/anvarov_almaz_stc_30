package almaz.homework7;

/**
 * демонстрирует работу паттерна Builder
 */
public class Main {

    public static void main(String[] args) {
        User user = User.builder()
                .firstName("Almaz")
                .lastName("Anvarov")
                .age(31)
                .isWorker(true)
                .build();
    }
}
