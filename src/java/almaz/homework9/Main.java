package almaz.homework9;

import java.util.Arrays;

/**
 * Демонстриует работу классов и интерфейсов
 */
public class Main {

    public static void main(String[] args) {

        String[] strings = {"Привет", "123HelloWorld!", "большими"};
        int[] numbers = {2345, 606060000, 76767};

        NumbersAndStringProcess numbersAndStringProcess = new NumbersAndStringProcess(strings, numbers);

        // развернуть исходное число
        NumbersProcess reverseNumber = number -> {
            int reversedNumber = 0;
            int copyNumber = number;
            while (copyNumber > 0) {
                reversedNumber = reversedNumber * 10 + (copyNumber % 10);
                copyNumber /= 10;
            }
            return reversedNumber;
        };

        //убрать нули из исходного числа
        NumbersProcess removeZero = number -> {
            int result = 0;
            while (number > 0) {
                if (number % 10 > 0) {
                    result *= 10;
                    result += number % 10;
                }
                number /= 10;
            }
            return result;
        };

        //заменить четные цифры ближайшей нечетной снизу (например, 7 на 6)
        NumbersProcess replaceEvenNumbers = new NumbersProcess() {
            @Override
            public int process(int number) {
                int result = 0;
                while (number > 0) {
                    if ((number % 10) % 2 != 0) {
                        result *= 10;
                        result += (number % 10) - 1;
                    } else {
                        result *= 10;
                        result += number % 10;
                    }
                    number /= 10;
                }
                return result;
            }
        };

        //развернуть исходную строку
        StringProcess reverseString = string -> {
            String result = "";
            for (int i = 0; i < string.length(); i++) {
                result = string.charAt(i) + result;
            }
            return result;
        };

        //убрать все цифры из строки
        StringProcess removeNumbers = string -> string.replaceAll("\\d", "");

        //сделать все маленькие буквы БОЛЬШИМИ
        StringProcess toUpperCase = string -> string.toUpperCase();

        System.out.println(Arrays.toString(numbersAndStringProcess.process(reverseNumber)));
        System.out.println(Arrays.toString(numbersAndStringProcess.process(reverseString)));

        System.out.println(Arrays.toString(numbersAndStringProcess.process(removeZero)));
        System.out.println(Arrays.toString(numbersAndStringProcess.process(removeNumbers)));

        System.out.println(Arrays.toString(numbersAndStringProcess.process(replaceEvenNumbers)));
        System.out.println(Arrays.toString(numbersAndStringProcess.process(toUpperCase)));
    }
}
