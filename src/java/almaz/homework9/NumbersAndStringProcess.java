package almaz.homework9;

/**
 * Выполняет приобразование массивов:
 */
public class NumbersAndStringProcess {

    String[] strings;
    int[] numbers;

    public NumbersAndStringProcess(String[] strings, int[] numbers) {
        this.strings = strings;
        this.numbers = numbers;
    }

    /**
     * Для демонстрации работы интерфейса
     *
     * @param stringProcess
     * @return
     */
    String[] process(StringProcess stringProcess) {
        String[] result = new String[3];

        for (int i = 0; i < strings.length; i++) {
            result[i] = stringProcess.process(strings[i]);
        }

        return result;
    }

    /**
     * Для демонстрации работы интерфейса
     *
     * @param numbersProcess
     * @return
     */
    int[] process(NumbersProcess numbersProcess) {
        int[] result = new int[3];

        for (int i = 0; i < numbers.length; i++) {
            result[i] = numbersProcess.process(numbers[i]);
        }

        return result;
    }
}
