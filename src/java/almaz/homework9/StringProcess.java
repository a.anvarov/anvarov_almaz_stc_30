package almaz.homework9;

/**
 * Интерфейс для работы над строками
 */
public interface StringProcess {
    String process(String string);
}
