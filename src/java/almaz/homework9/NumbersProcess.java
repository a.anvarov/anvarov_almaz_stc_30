package almaz.homework9;

/**
 * Интерфейс для работы над числами
 */
public interface NumbersProcess {
    int process(int number);
}
