package almaz.homework2.case7;

import java.util.Scanner;

/**
 * программа выводит в консоль массив в котором элементы массива закручены в спираль,
 * размерность массива вводиться с клавиатуры
 * m - число строк
 * n - число столбцов
 */
public class Program {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Line  m = ");
        int m = scanner.nextInt();

        System.out.println("Column  n = ");
        int n = scanner.nextInt();

        int array[][] = new int[m][n];

        int number = 0;

        int iMinimal, jMinimal, iMaximal, jMaximal;
        jMinimal = 0;
        iMinimal = 1;
        jMaximal = n - 1;
        iMaximal = m - 1;

        while (number < n * m) {
            for (int j = jMinimal; j <= jMaximal; j++) {
                number++;
                array[iMinimal - 1][j] = number;
            }
            for (int i = iMinimal; i <= iMaximal; i++) {
                number++;
                array[i][jMaximal] = number;
            }
            jMaximal--;
            for (int j = jMaximal; j >= jMinimal; j--) {
                number++;
                array[iMaximal][j] = number;
            }
            iMaximal--;
            for (int i = iMaximal; i >= iMinimal; i--) {
                number++;
                array[i][jMinimal] = number;
            }
            iMinimal++;
            jMinimal++;
        }

// Вывод на экран
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print("[" + i + "," + j + "] ");
            }
            System.out.println();
        }
        System.out.println();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}
