package almaz.homework2.case5;

import java.util.Arrays;
import java.util.Scanner;

/**
 * программа выводит в консоль массив который сортируется методом пузырьковой сортировки,
 * массив вводиться с клавиатуры
 */
public class Program {

    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int[n];

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        System.out.println("Array from Console: " + Arrays.toString(array));

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int swapVariable = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = swapVariable;
                }
            }
        }

        System.out.println("Sorted Array      : " + Arrays.toString(array));
    }
}