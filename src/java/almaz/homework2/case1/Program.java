package almaz.homework2.case1;

import java.util.Scanner;

/**
 * программа выводит в консоль сумму элементов массива,
 * массив вводиться с клавиатуры
 */
public class Program {

    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int[n];
        int arraySum = 0;

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        for (int i = 0; i < array.length; i++) {
            arraySum += array[i];
        }

        System.out.println(arraySum);
    }
}