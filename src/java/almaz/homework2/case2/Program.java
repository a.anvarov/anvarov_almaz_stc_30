package almaz.homework2.case2;

import java.util.Arrays;
import java.util.Scanner;

/**
 * программа выводит в консоль массив который развернули в обратном порядке,
 * массив вводиться с клавиатуры
 */
public class Program {

    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int[n];

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        int bufferVariable;
        for (int i = 0; i < (array.length / 2); ++i) {
            bufferVariable = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = bufferVariable;
        }

        System.out.println(Arrays.toString(array));
    }
}