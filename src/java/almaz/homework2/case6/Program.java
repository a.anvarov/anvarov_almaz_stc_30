package almaz.homework2.case6;

/**
 * программа выводит в консоль результат преобразования массива в число
 */
public class Program {

    public static void main(String args[]) {
        int array[] = {4, 2, 3, 5, 7};
        int number = 0;

        for (int i = 0; i < array.length; i++) {
            number *= 10;
            number += array[i];
        }

        System.out.println(number);
    }
}