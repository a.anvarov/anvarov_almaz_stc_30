package almaz.homework2.case4;

import java.util.Arrays;
import java.util.Scanner;

/**
 * программа выводит в консоль массив в котором поменялись местами максимальный и минимальный элементы,
 * массив вводиться с клавиатуры
 */
public class Program {

    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int[n];

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        System.out.println("Original array: " + Arrays.toString(array));

        int minIndex = 0;
        int maxIndex = 0;

        int min = array[0];
        int max = array[0];

        for (int i = 0; i < array.length; i++) {
            if (min <= array[i]) {
                min = array[i];
                minIndex = i;
            }
            if (max >= array[i]) {
                max = array[i];
                maxIndex = i;
            }
        }

        int swapVariable = array[minIndex];
        array[minIndex] = array[maxIndex];
        array[maxIndex] = swapVariable;

        System.out.println("Result array:   " + Arrays.toString(array));
    }
}