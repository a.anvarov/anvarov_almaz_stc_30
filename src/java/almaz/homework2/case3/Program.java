package almaz.homework2.case3;

import java.util.Scanner;

/**
 * программа выводит в консоль среднее арифметическое элементов массива - введенного с клавиатуры
 */
public class Program {

    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int[n];
        double arrayAverage;

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        int arraySum = 0;
        for (int i = 0; i < array.length; i++) {
            arraySum += array[i];
        }

        arrayAverage = arraySum * 1.0 / n;

        System.out.println(arrayAverage);
    }
}