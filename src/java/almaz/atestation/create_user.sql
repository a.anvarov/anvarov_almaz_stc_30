create table gm_user
(
    id             serial primary key,
    ip             varchar(16),
    first_name     varchar(20),
    max_point      int,
    win_lose_count int
);

create table gm_game
(
    id                 serial primary key,
    game_date          timestamp,
    shoot_gamers_count int,
    game_time          timestamp
);

create table gm_shoot
(
    id              serial primary key,
    shoot_time      timestamp,
    hit             bool,
    shooter_user_id int,
    hit_user_id     int,
    foreign key (shooter_user_id) references gm_user (id),
    foreign key (hit_user_id) references gm_user (id)
);