package almaz.homework14;

/**
 * Модель данных Информация по Автомобилям
 */
public class CarInfo {

    /**
     * Номер автомобиля
     */
    private String number;
    /**
     * Модель автомобиля
     */
    private String model;
    /**
     * Цвет автомобиля
     */
    private String colour;
    /**
     * Пробег автомобиля
     */
    private Long mileage;
    /**
     * Стоимость автомобиля
     */
    private Long cost;

    public CarInfo() {
    }

    public CarInfo(String number, String model, String colour, Long mileage, Long cost) {
        this.number = number;
        this.model = model;
        this.colour = colour;
        this.mileage = mileage;
        this.cost = cost;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public Long getMileage() {
        return mileage;
    }

    public void setMileage(Long mileage) {
        this.mileage = mileage;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }
}
