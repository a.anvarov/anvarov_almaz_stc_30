package almaz.homework14;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import static java.nio.file.Files.lines;

/**
 * Демонстрирует работу Stream API
 */
public class Main {

    private static final Path PATH = Paths.get("src/java/almaz/homework14/car_info.txt"); // TODO константы пишутся капсом EXAMPLE_NAME_OF_CONST //DONE

    public static void main(String[] args) {

        try {
            //Номера всех автомобилей, имеющих черный цвет или нулевой пробег.
            List<String> carNumbers = lines(PATH)
                    .map(line -> line.split(" "))
                    .map(array -> new CarInfo(array[0], array[1], array[2], Long.parseLong(array[3]), Long.parseLong(array[4])))
                    .filter(carInfo -> carInfo.getColour().equals("Black") || carInfo.getMileage() == 0)
                    .map(carInfo -> carInfo.getNumber())
                    .collect(Collectors.toList());

            System.out.println(Arrays.asList(carNumbers));

            //Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.
            long countUniqueCarModels = lines(PATH)
                    .map(line -> line.split(" "))
                    .map(array -> new CarInfo(array[0], array[1], array[2], Long.parseLong(array[3]), Long.parseLong(array[4])))
                    .filter(carInfo -> carInfo.getCost() >= 700000L && carInfo.getCost() <= 800000L)
                    .map(carInfo -> carInfo.getModel())
                    .distinct()
                    .count();

            System.out.println(Arrays.asList(countUniqueCarModels));

            //Вывести цвет автомобиля с минимальной стоимостью
            Optional<CarInfo> carColour = lines(PATH)
                    .map(line -> line.split(" "))
                    .map(array -> new CarInfo(array[0], array[1], array[2], Long.parseLong(array[3]), Long.parseLong(array[4])))
                    .min(minComparator);

            System.out.println(carColour.get().getModel());

            //Среднюю стоимость Camry.
            OptionalDouble averageCostCamry = lines(PATH)
                    .map(line -> line.split(" "))
                    .map(array -> new CarInfo(array[0], array[1], array[2], Long.parseLong(array[3]), Long.parseLong(array[4])))
                    .filter(carInfo -> carInfo.getModel().equals("Toyota_Camry"))
                    .map(carInfo -> carInfo.getCost())
                    .mapToLong(i -> i)
                    .average();

            System.out.println(averageCostCamry.getAsDouble());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static Comparator<CarInfo> minComparator = Comparator.comparing(CarInfo::getCost);
}
