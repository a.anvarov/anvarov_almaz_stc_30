package almaz.homework11;

public interface Iterable<B> {
    Iterator<B> iterator();
}
