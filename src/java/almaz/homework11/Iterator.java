package almaz.homework11;

public interface Iterator<A> {
    A next();
    boolean hasNext();
}