package almaz.homework11;

public class LinkedList<E> implements List<E> {

    private class LinkedListIterator implements Iterator<E> {

        @Override
        public E next() {
            return null;
        }

        @Override
        public boolean hasNext() {
            return false;
        }
    }

    private Node<E> first;
    private Node<E> last;

    private int count;

    private static class Node<F> {
        F value;
        Node<F> next;

        public Node(F value) {
            this.value = value;
        }
    }

    @Override
    public E get(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node<E> current = this.first;

            while (i < index) {
                current = current.next;
                i++;
            }

            return current.value;
        }
        System.err.println("Такого элемента нет");
        return null;
    }

    @Override
    public int indexOf(E element) {
        int i = 0;
        Node<E> current = this.first;

        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }

        if (current == null) {
            return -1;
        } else {
            return i;
        }
    }

    @Override
    public void removeByIndex(int index) {
        Node<E> node = first;

        if (index == 0) {
            first = node.next;
            count--;
        } else {
            for (int i = 0; node != null && i < index - 1; i++) {
                node = node.next;
            }
            if (nodeCheckByIndex(node, index)) {
                Node<E> next = node.next.next;
                node.next = next;
                count--;
            }
        }
    }

    @Override
    public void insert(E element, int index) {
        Node<E> elementNode = new Node(element);

        if (index == 0) {
            elementNode.next = first.next;
            first = elementNode;
            count++;
        } else {
            Node<E> node = first;
            for (int i = 0; node != null && i < index - 1; i++) {
                node = node.next;
            }
            if (nodeCheckByIndex(node, index)) {
                node = elementNode;
                elementNode.next = node.next;
                count++;
            }
        }
    }

    private boolean nodeCheckByIndex(Node node, int index) {
        if (node == null || node.next == null) {
            System.err.println(String.format("Под данным индексом %s не содержиться элемент", index));
            return false;
        }
        return true;
    }

    @Override
    public void add(E element) {
        Node<E> newNode = new Node<>(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            last.next = newNode;
            last = newNode;
        }
        count++;
    }

    @Override
    public boolean contains(E element) {
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeFirst(E element) {
        Node<E> previousNode = null;
        Node<E> currentNode = first;

        while (currentNode != null) {
            if (currentNode.value == element) {
                previousNode.next = currentNode.next;
                count--;
            }
            previousNode = currentNode;
            currentNode = currentNode.next;
        }
    }

    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }
}
