package almaz.homework1.case1;

/**
 * программа выводит в консоль сумму цифр пятизначного числа
 */
public class Program {

    public static void main(String[] args) {
        int number = 12345;

        int digitsSum = number % 10;
        number /= 10;

        digitsSum += number % 10;
        number /= 10;

        digitsSum += number % 10;
        number /= 10;

        digitsSum += number % 10;
        number /= 10;

        digitsSum += number % 10;

        System.out.println(digitsSum);
    }
}