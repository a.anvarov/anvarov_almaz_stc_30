package almaz.homework1.case2;

/**
 * программа выводит в консоль двоичное представление пятизначного числа
 */
public class Program {

    public static void main(String[] args) {
        int number = 12345;

        int v1 = number % 2 == 0 ? 0 : 1;
        number /= 2;

        int v2 = number % 2 == 0 ? 0 : 1;
        number /= 2;

        int v3 = number % 2 == 0 ? 0 : 1;
        number /= 2;

        int v4 = number % 2 == 0 ? 0 : 1;
        number /= 2;

        int v5 = number % 2 == 0 ? 0 : 1;
        number /= 2;

        int v6 = number % 2 == 0 ? 0 : 1;
        number /= 2;

        int v7 = number % 2 == 0 ? 0 : 1;
        number /= 2;

        int v8 = number % 2 == 0 ? 0 : 1;
        number /= 2;

        int v9 = number % 2 == 0 ? 0 : 1;
        number /= 2;

        int v10 = number % 2 == 0 ? 0 : 1;
        number /= 2;

        int v11 = number % 2 == 0 ? 0 : 1;
        number /= 2;

        int v12 = number % 2 == 0 ? 0 : 1;
        number /= 2;

        int v13 = number % 2 == 0 ? 0 : 1;
        number /= 2;

        int v14 = number % 2 == 0 ? 0 : 1;

        long binaryNumber = v14;

        if (v13 == 1) {
            binaryNumber *= 10;
            binaryNumber += 1;
        } else {
            binaryNumber *= 10;
        }
        if (v12 == 1) {
            binaryNumber *= 10;
            binaryNumber += 1;
        } else {
            binaryNumber *= 10;
        }
        if (v11 == 1) {
            binaryNumber *= 10;
            binaryNumber += 1;
        } else {
            binaryNumber *= 10;
        }
        if (v10 == 1) {
            binaryNumber *= 10;
            binaryNumber += 1;
        } else {
            binaryNumber *= 10;
        }
        if (v9 == 1) {
            binaryNumber *= 10;
            binaryNumber += 1;
        } else {
            binaryNumber *= 10;
        }
        if (v8 == 1) {
            binaryNumber *= 10;
            binaryNumber += 1;
        } else {
            binaryNumber *= 10;
        }
        if (v7 == 1) {
            binaryNumber *= 10;
            binaryNumber += 1;
        } else {
            binaryNumber *= 10;
        }
        if (v6 == 1) {
            binaryNumber *= 10;
            binaryNumber += 1;
        } else {
            binaryNumber *= 10;
        }
        if (v5 == 1) {
            binaryNumber *= 10;
            binaryNumber += 1;
        } else {
            binaryNumber *= 10;
        }
        if (v4 == 1) {
            binaryNumber *= 10;
            binaryNumber += 1;
        } else {
            binaryNumber *= 10;
        }
        if (v3 == 1) {
            binaryNumber *= 10;
            binaryNumber += 1;
        } else {
            binaryNumber *= 10;
        }
        if (v2 == 1) {
            binaryNumber *= 10;
            binaryNumber += 1;
        } else {
            binaryNumber *= 10;
        }
        if (v1 == 1) {
            binaryNumber *= 10;
            binaryNumber += 1;
        } else {
            binaryNumber *= 10;
        }

        System.out.println(binaryNumber);
    }
}