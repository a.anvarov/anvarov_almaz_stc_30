package almaz.homework1.case3;

import java.util.Scanner;

/**
 * выводит в консоль для заданной последовательности чисел считает
 * произведение тех чисел, сумма цифр которых - простое число. Последнее число
 * последовательности - 0
 */
public class Program {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int consoleNumber;

        consoleNumber = scanner.nextInt();

        long result = 1;

        while (consoleNumber != 0) {
            int currentNumber = consoleNumber;

            int digitsSum = 0;

            while (consoleNumber > 0) {
                digitsSum += consoleNumber % 10;
                consoleNumber /= 10;
            }

            boolean primeNumber = true;

            for (int i = 2; i < digitsSum; i++) {
                if (digitsSum % i == 0) {
                    primeNumber = false;
                    break;
                }
            }

            if (primeNumber) {
                result *= currentNumber;
            }

            consoleNumber = scanner.nextInt();
        }

        System.out.println(result);
    }
}