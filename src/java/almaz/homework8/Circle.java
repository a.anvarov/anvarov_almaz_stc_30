package almaz.homework8;

/**
 * круг геометрическая фигура
 */
public class Circle extends Ellipse {

    public Circle() {
    }

    public Circle(double radius) {
        super(radius, radius);
    }
}
