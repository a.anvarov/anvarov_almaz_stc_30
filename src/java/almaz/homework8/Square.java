package almaz.homework8;

/**
 * квадрат геометрическая фигура
 */
public class Square extends Rectangle {

    public Square() {
    }

    public Square(double side) {
        super(side, side);
    }
}
