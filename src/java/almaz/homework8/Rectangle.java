package almaz.homework8;

/**
 * прямоугольник геометрическая фигура
 */
public class Rectangle extends AbstractFigure implements AreaPerimeterFigure {

    private double width;
    private double height;

    public Rectangle() {
    }

    public Rectangle(double height, double width) {
        this.height = height;
        this.width = width;
    }

    @Override
    public double area() {
        return height * width;
    }

    @Override
    public double perimeter() {
        return 2 * (height + width);
    }

    @Override
    public void relocate(int x, int y) {
        point.x += x;
        point.y += y;
    }

    @Override
    public void scale(int scaleNumber) {
        height *= scaleNumber;
        width *= scaleNumber;
    }
}
