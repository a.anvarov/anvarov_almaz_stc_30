package almaz.homework8;

/**
 * для перемещения фигуры
 */
public interface Relocatable {

    /**
     * перемещает фигуру
     *
     * @param x ордината центра фигуры
     * @param y абсцисса центра фигуры
     */
    void relocate(int x, int y);
}
