package almaz.homework8;

/**
 * эллипм геометрическая фигура
 */
public class Ellipse extends AbstractFigure implements AreaPerimeterFigure {

    private double semiMajorRadiusA;
    private double semiMinorRadiusB;

    public Ellipse() {
    }

    public Ellipse(double semiMajorRadiusA, double semiMinorRadiusB) {
        this.semiMajorRadiusA = semiMajorRadiusA;
        this.semiMinorRadiusB = semiMinorRadiusB;
    }

    @Override
    public double area() {
        return semiMajorRadiusA * semiMinorRadiusB * PI;
    }

    @Override
    public double perimeter() {
        return 4 * (((PI * semiMajorRadiusA * semiMinorRadiusB) + (semiMajorRadiusA - semiMinorRadiusB)) / (semiMajorRadiusA + semiMinorRadiusB));
    }

    @Override
    public void relocate(int x, int y) {
        point.x += x;
        point.y += y;
    }

    @Override
    public void scale(int scaleNumber) {
        semiMajorRadiusA *= scaleNumber;
        semiMinorRadiusB *= scaleNumber;
    }
}
