package almaz.homework8;

/**
 * абстрактная фигура
 */
public abstract class AbstractFigure implements AreaPerimeterFigure, Scalable, Relocatable {

    /**
     * приблеженное значение числа pi
     */
    protected final double PI = Math.PI;

    protected Point point;

    public Point getPoint() {
        return point;
    }
    public void setPoint(Point point) {
        this.point = point;
    }
}
