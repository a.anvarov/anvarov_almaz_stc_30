package almaz.homework8;

/**
 * для масштабирования фигуры
 */
public interface Scalable {

    /**
     * масштабирует фигуру
     *
     * @param scaleNumber
     * @return фигуру
     */
    void scale(int scaleNumber);
}
