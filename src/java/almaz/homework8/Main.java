package almaz.homework8;

public class Main {

    public static void main(String[] args) {
        Point pointCircle = new Point(0, 5);
        Circle circle = new Circle(5.0);
        circle.setPoint(pointCircle);
        System.out.print("Площадь Круга: " + circle.area());
        System.out.println(" Периметер Круга: " + circle.perimeter());

        Point pointEllipse = new Point(0, 5);
        Ellipse ellipse = new Ellipse(4.0, 5.0);
        ellipse.setPoint(pointEllipse);
        System.out.print("Площадь Элипса: " + ellipse.area());
        System.out.println(" Периметер Элипса: " + ellipse.perimeter());

        Point pointSquare = new Point(0, 5);
        Square square = new Square(4.0);
        square.setPoint(pointSquare);
        System.out.print("Площадь Квадрата: " + square.area());
        System.out.println(" Периметер Квадрата: " + square.perimeter());


        Point pointRectangle = new Point(0, 5);
        Rectangle rectangle = new Rectangle(4.0, 5.0);
        rectangle.setPoint(pointRectangle);
        rectangle.scale(5);
        rectangle.relocate(1, 2);
        System.out.print("Площадь Прямоугольника: " + rectangle.area());
        System.out.println(" Периметер Прямоугольника: " + rectangle.perimeter());
        System.out.println("Центр Прямоугольника: " + rectangle.getPoint().getX() + ", " + rectangle.getPoint().getY());
    }
}
