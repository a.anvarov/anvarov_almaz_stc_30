package almaz.homework8;

/**
 * предоставляет методы для вычисления Площади и Периметра - фигуры
 */
public interface AreaPerimeterFigure {

    /**
     * вычисляет площадь фигуры
     *
     * @return площадь фигуры
     */
    double area();

    /**
     * вычисляет периметер фигуры
     *
     * @return периметер фигуры
     */
    double perimeter();
}
